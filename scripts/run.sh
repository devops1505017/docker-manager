
#!/bin/bash
# for ctx in "${HOSTS[@]}" "${PLATFORM_HOSTS[@]}" "${PROD_HOSTS[@]}"; do

for ctx in "${HOST[@]}"; do
    for host in ${ctx[@]}; do
        echo -e '\033[0;31m'RUNNING ON $host'\033[0m'
        HOST=$host sh $1
    done;
done;
