context:
	docker context create home --docker host=ssh://SkyNet && \
	docker context create prod --docker host=ssh://Prod;

network-prod:
	docker context use prod && \
	docker network create -d bridge my-bridge-network;

certbot-home:
	cd nginx && \
	docker context use home && \
	docker compose -f certbot-home.yml down; \
	docker compose -f certbot-home.yml up -d;

certbot-prod:
	cd nginx && \
	docker context use prod && \
	docker compose -f certbot-prod.yml down; \
	docker compose -f certbot-prod.yml up -d;

dev-platform:
	cd dev && \
	docker context use home && \
	docker compose -f docker-compose.yml down; \
	docker compose -f docker-compose.yml up -d;

gl-runner:
	docker context use hostVDS && \
	docker compose -f service.yml down; \
	docker compose -f service.yml up -d;

nextcloud-home:
	cd nextcloud && \
	docker context use home && \
	docker compose -f service.yml down; \
	docker compose -f service.yml up -d;

nginx-home:
	cd nginx && \
	docker context use home && \
	docker compose -f service.yml -f home.yml down; \
	docker compose -f service.yml -f home.yml up -d;

nginx-prod:
	cd nginx && \
	docker context use prod && \
	docker compose -f service.yml -f prod.yml down; \
	docker compose -f service.yml -f prod.yml up -d;

portainer-home:
	cd portainer && \
	docker context use home && \
	docker compose -f service.yml down; \
	docker compose -f service.yml up -d;

portainer-prod:
	cd portainer-agent && \
	docker context use prod && \
	docker compose -f service.yml down; \
	docker compose -f service.yml up -d;

3x-ui-prod:
	cd vpn && \
	docker context use prod && \
	docker compose -f service.yml -f 3x-ui.yml -f 3x-ui-prod.yml down; \
	docker compose -f service.yml -f 3x-ui.yml -f 3x-ui-prod.yml up -d;

mariadb-prod:
	cd maria_DB && \
	docker context use prod && \
	docker compose -f mariadb.yml down; \
	docker compose -f mariadb.yml --env-file .env up -d;

mariadb-dev:
	cd maria_DB && \
	docker context use prod && \
	docker compose -f mariadb-dev.yml down; \
	docker compose -f mariadb-dev.yml --env-file .env-dev up -d;

adminer-prod:
	cd maria_DB && \
	docker context use prod && \
	docker compose -f adminer.yml down; \
	docker compose -f adminer.yml up -d;